# Open Weather App

A simple app to show weather for location from select default which can be easily modified

### Libraries Used
* Bootstrap
* Angular

### APIs Used
* Open Weather

### Running Application
* Application can be run using any simple http server
* One way to simply run this would be using http-server node package
```
npm install -g http-server
Go to app/
http-server -o
```

## Highlights
* Modular design with controllers, directives, filters, pages, css in their respectively files and well segregated. Allows for segregation of responsibility.
* Entry point to the application is index.html
* Main javascript file being app.js which is responsible for registering all other modules with app
* Currently, it has only one route but new routes can be easily added in app.js 

## Screenshots
<img src="app/img/screenshot.png">

## Future possible enhancements
* Allowing location selection from wider available location preset
* Allow weather search for any valid location
* Minification of HTML, CSS & JavaScript files using grunt or gulp
* Uglification of JavaScript files using grunt or gulp
* Improving the UX for application
* Adding unit test cases using jasmine and karma
