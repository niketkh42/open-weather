(function(){
	'use strict';
	angular.module('openWeatherApp', [
  		'ngRoute',
	  	'openWeatherApp.filters',
	  	'openWeatherApp.services',
	  	'openWeatherApp.directives',
	  	'openWeatherApp.controllers'
	]).
	config(['$routeProvider', function($routeProvider) {
	  $routeProvider.when('/forecast', {templateUrl: 'pages/forecast.html', controller: 'OpenWeatherCtrl'});
	  $routeProvider.otherwise({redirectTo: '/forecast'});
	}]);

})();