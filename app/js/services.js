(function(){
  'use strict';
  angular.module('openWeatherApp.services', ['ngResource'])
    .value('locations',['London','France','Germany','Italy','Spain'])
    .factory('openWeatherMap', function($resource) {
      var apiKey = '3d8b309701a13f65b660fa2c64cdc517';
      var apiBaseUrl = 'http://api.openweathermap.org/data/2.5/';

      return $resource(apiBaseUrl + ':path/:subPath?q=:location',
        {
          APPID: apiKey,
          mode: 'json',
          callback: 'JSON_CALLBACK',
          units: 'metric',
          lang: 'en'
        },
        {
          queryWeather: {
            method: 'JSONP',
            params: {
              path: 'weather'
            },
            isArray: false,
          },
          queryForecastDaily: {
            method: 'JSONP',
            params: {
              path: 'forecast',
              subPath: 'daily',
              cnt: 5
            },
            isArray: false,
          }
        }
      )
    }); 

})();