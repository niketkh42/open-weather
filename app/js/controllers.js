(function(){
  'use strict';
  angular.module('openWeatherApp.controllers', [])

  .controller('OpenWeatherCtrl',
    ['$scope', 'openWeatherMap','locations',
      function($scope, openWeatherMap, locations) {

    $scope.message = '';
    $scope.hasState = '';

    $scope.locations = locations;
    $scope.iconBaseUrl = 'http://openweathermap.org/img/w/';

    $scope.forecast = openWeatherMap.queryForecastDaily({
      location: locations[0]
    });

    $scope.getForecastByLocation = function() {

      if ($scope.location == '' || $scope.location == undefined) {
        $scope.hasState = 'has-warning';
        $scope.message = 'Please provide a location';
        return;
      }

      $scope.hasState = 'has-success';

      $scope.forecast = openWeatherMap.queryForecastDaily({
        location: $scope.location
      });
    };

    // Set $scope.location and execute search on API
    $scope.setLocation = function(location) {
      $scope.location = location;
      $scope.getForecastByLocation();
    };

    // Get icon image url
    $scope.getIconImageUrl = function(iconName) {
      return (iconName ? $scope.iconBaseUrl + iconName + '.png' : '');
    };

  }])

})();