(function(){
  'use strict';
  angular.module('openWeatherApp.filters', [])
    .filter('placeholder', [function() {
      return function (input, placeholder) {
        return (angular.isUndefined(input) || input == '') ? placeholder : input;
      };
    }])

})();